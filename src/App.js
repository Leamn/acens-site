import './App.css';
import Navbar from "./components/navbar";
import Home from "./components/home";
import Servicos from "./components/servicos";
import Values from './components/values';
import Explorers from './components/explorers';
import Projects from './components/projects';
import Contact from './components/contact';
import Footer from './components/footer';
import Button from './components/navbar/end-button';

function App() {
  
  const sections = document.querySelectorAll("section");
    const nav = document.querySelectorAll(".Navbar .Container .Menu a");


    window.addEventListener("scroll", () => {
      let current = "";
      sections.forEach((section) => {
        const sectionTop = section.offsetTop;
        const sectionHeight = section.clientHeight;
        if (window.scrollY >= sectionTop - sectionHeight/3) {
          current = section.getAttribute("id");
        }
      });

      nav.forEach((a) => {
        a.classList.remove("On");
        if (a.classList.contains(current)) {
          a.classList.add("On");
        }
      });
    });


  return (
    <div className="App">
      <Navbar />
      <Home />
      <Servicos />
      <Values />
      <Explorers />
      <Projects />
      <Contact />
      <Footer />
      <Button />
    </div>
  );
}

export default App;
