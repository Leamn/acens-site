import "./style.css";
import { useState } from "react";

function Navbar() {

    const [navbar, setNavbar] = useState(false);

    const changeNavbar = () => {
      if (window.scrollY >= 80) {
        setNavbar(true);
      } else {
        setNavbar(false);
      }
    };

    window.addEventListener('scroll', changeNavbar)

    return (
      <div className="Navbar">
        <div className={navbar ? 'Navbar Active' : 'Navbar'}>
          <div className="Container">
            <a href="/">Acens</a>
            <div className="Menu">
              <a className="Home On" href="#Home">INÍCIO</a>
              <a className="Servicos" href="#Servicos">SERVIÇOS</a>
              <a className="Explorers" href="#Explorers">EQUIPE</a>
              <a className="Projects" href="#Projects">PORTFÓLIO</a>
              <a className="Contact" href="#Contact">CONTATO</a>
            </div>
          </div>
        </div>
      </div>
    );
  }

  export default Navbar;
