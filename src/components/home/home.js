import "./style.css";

function Home() {


    return (
      <section className="Home" id="Home">
          <div className="Section-1">
            <div className="Cover-home"></div>
            <div className="Container">
              <div className="Intro">
                <div className="Intro-items">
                  <div className="Intro-item">
                   <h1>ACENS</h1>
                    <h3>A REALIZAÇÃO DA SUA APLICAÇÃO COMEÇA AQUI.</h3>
                    <a href="#Section-2">Saiba mais</a>
                  </div>
                  <div className="Intro-item">
                    <h1>MAIS DE 10 ANOS NO MERCADO</h1>
                    <h3>DESENVOLVENDO SITES E SOLUÇÕES WEB</h3>
                    <a href="#Servicos">Veja nossos serviços</a>
                  </div>
                  <div className="Intro-item">
                    <h1>PRONTO PARA NOS CONHECER?</h1>
                    <h3>NÃO PERCA TEMPO!</h3>
                    <a href="#Section-2">Conheça-nos</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="Section-2" id="Section-2">
            <div className="Container">
            <div className="About">
              <div className="About-text">
                <h2>{"{PROJETOS COM CRIATIVIDADE AO SEU ALCANCE}"}</h2>
                <p>Somos uma associação sem fins lucrativos, que visa impactar empreendedores com projetos de baixo custo e alta qualidade.</p>
              </div>
              <div className="About-button">
                <a href="#Contact">Entrar em contato!</a>
              </div>
            </div>
            </div>
          </div>
      </section>
    );
  }

  export default Home;
