import "./style.css";

function Values() {
  /*
  const mural = document.querySelector(".Mural");
  const infoOn = document.querySelectorAll(".Values .Container .Mural ul li a");

  mural.addEventListener("click", () => {
    let current = "";

    infoOn.forEach((a) => {
      a.classList.remove("On");
      if (a.classList.contains(current)) {
        a.classList.add("On");
      }
    });
  });
  */

    return (
      <div className="Values">
          <div className="Container">
          <div className="Mural">
            <ul>
                <li><a href="#tab-junior" role="tab" aria-controls="tab-junior" aria-expanded="true"><img src ="https://static.thenounproject.com/png/1201392-200.png"></img>Ser Júnior</a></li>
                <li><a href="#tab-visao" role="tab" aria-controls="tab-visao" aria-expanded="false"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Font_Awesome_5_solid_binoculars.svg/2048px-Font_Awesome_5_solid_binoculars.svg.png"></img>Visão</a></li>
                <li><a href="#tab-missao" role="tab" aria-controls="tab-missao" aria-expanded="false"><img src="https://cdn.pixabay.com/photo/2016/03/31/19/14/check-box-1294836_1280.png"></img>Missão</a></li>
                <li><a href="#tab-historia" role="tab" aria-controls="tab-historia" aria-expanded="false"><img src="https://i.pinimg.com/originals/46/01/f7/4601f773e41c094849e10288a7aec5e8.png"></img>História</a></li>
            </ul>
            <div className="Mural-content">
              <div className="Info ON" role="tabpanel" id="tab-junior">
                <h5>MOVIMENTO EMPRESA JÙNIOR</h5>
                <p>Empresa júnior é uma associação civil sem fins lucrativos e com fins educacionais formada exclusivamente por alunos do ensino superior ou Técnico. O objetivo primeiro das empresas juniores é desenvolver pessoal e profissionalmente os seus membros por meio da vivência empresarial...
                  <a href="/">Veja mais</a>
                </p>
              </div>
              <div className="Info" role="tabpanel" id="tab-visao">
                <p>Ser referência Web Mobile entre as empresas Juniores do Ceará, Investir mais em membros, Ser mais conectado com a Rede de empresários juniores.</p>
              </div>
              <div className="Info" role="tabpanel" id="tab-missao">
                <p>Formar líderes e profissionais, apaixonados por alta performance, desenvolvendo sonhos, por meio das nossas soluções Web Mobile.</p>
              </div>
              <div className="Info" role="tabpanel" id="tab-historia">
                <h5>JÚNIOR DE TI PIONEIRA NO CEARÁ</h5>
                <p>Fundada em 2008, a Acens foi criada por alunos da graduação de Ciência da Computação com o intuito de colocar em prática seus conhecimentos para desenvolver aplicações que o mercado exige. Ao longo das gestões, passamos por diversas mudanças que nos trouxeram experiências de enriquecimento...
                  <a href="/">Veja mais</a>
                </p>
              </div>
            </div>
          </div>
          <div className="Qualidades">
            <h3>Nossos valores</h3>
            <h5>AMOR PELA ACENS</h5>
            <div className="Bar"></div>
            <h5>EXPLORADOR</h5>
            <div className="Bar"></div>
            <h5>INCONFORMADO</h5>
            <div className="Bar"></div>
            <h5>ÉTICO</h5>
            <div className="Bar"></div>
            <h5>VÁ ALÉM</h5>
            <div className="Bar"></div>
            <h5>FOCO NO CLIENTE</h5>
            <div className="Bar"></div>
            <h5>COMPROMISSO DE CLÃ</h5>
            <div className="Bar"></div>
          </div>
          </div>
      </div>
    );
  }

  export default Values;
