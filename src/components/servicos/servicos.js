import "./style.css";

function Servicos() {
    return (
      <section className="Servicos" id="Servicos">
          <div className="Section-3">
          <div className="Cover-servicos"></div>
            <div className="Container">
              <div className="Servicos-title">
                <h1>O QUE NÓS FAZEMOS</h1>
                <div className="Divider">
                  <div className="Line"></div>
                  <div className="Div">
                    <p>//</p>
                  </div>
                  <div className="Line"></div>
                </div>
                <p>Desenvolvemos soluções para atender a sua real necessidade,<br/> com um design único e morderno!</p>
              </div>
              <div className="Servicos-itens">
                <div className="Servicos-item">
                  <div className="Servicos-item-title">
                    <span><img src="https://flaticons.net/icon.php?slug_category=miscellaneous&slug_icon=paint-brush-01" alt="brush"></img></span>
                    <h4>Sites</h4>
                  </div>
                  <div>
                    <p>Páginas que se adaptam a qualquer tamanho de tela e a todos os dipositivos móveis, com rápido desempenho e de fácil acesso.</p>
                  </div>
                </div>

                <div className="Servicos-item">
                  <div className="Servicos-item-title">
                    <span><img src="https://imvery.moe/img/Coding%20app.png" alt="code" ></img></span>
                    <h4>Sistemas</h4>
                  </div>
                  <div>
                    <p>Automatize os processos internos de sua empresa ou tenha aplicações on-line para a realização de cadastros, vendas e outras interações com seus clientes.</p>
                  </div>
                </div>

                <div className="Servicos-item">
                  <div className="Servicos-item-title">
                    <span><img src="https://flaticons.net/icon.php?slug_category=miscellaneous&slug_icon=wrench" alt="wrench"></img></span>
                    <h4>Manutenção</h4>
                  </div>
                  <div>
                    <p>Fornecemos manutenção para suas aplicações, afinal de contas, queremos te dar todo o suporte que precisar.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </section>
    );
  }
  
  export default Servicos;