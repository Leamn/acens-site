import "./style.css";

function Footer() {
    return (
      <div className="Footer">
        <div className="Container">
        <p>2021 © Acens. Todos os direitos reservados</p>
        <div className="Socials">
          <a href="https://www.facebook.com/acensjunior"><img src="https://www.allwood.com.br/loja/wp-content/uploads/2019/02/facebook-icon-18-256.png" alt="Facebook"></img></a>
          <a href="https://www.instagram.com/acensjr/"><img src="https://pngimage.net/wp-content/uploads/2018/06/logo-ig-png-12.png" alt="Instagram"></img></a>
          <a href="mailto:contato@acensjr.com"><img src="https://cdn.pixabay.com/photo/2013/07/13/01/16/newsgroup-155430_960_720.png" alt="Email"></img></a>
        </div>
        </div>
      </div>
    );
  }
  
  export default Footer;