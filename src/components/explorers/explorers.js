import Members from "./members";
import "./style.css";

function Explorers() {
    return (
      <section className="Explorers" id="Explorers">
        <div className="Container">
        <div className="Explorers-title">
            <h1>NOSSOS EXPLORADORES</h1>
            <div className="Divider">
              <div className="Line"></div>
              <div className="Div">
                <p>//</p>
              </div>
              <div className="Line"></div>
            </div>
            <p>Contamos com uma equipe capacitada e apaixonada pelo o que faz, utilizando das tecnologias mais recentes do mercado.</p>
        </div>
        <Members />
        </div>
      </section>
    );
  }
  
  export default Explorers;
  