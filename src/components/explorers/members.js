import "./style.css";
import { useEffect, useState} from "react";
import {Image} from 'cloudinary-react';

function Members() {

    const [explorers, setExplorers] = useState([]);
    
    const Member = [
        {
          id: 1,
          image:"https://res.cloudinary.com/dddogptqy/image/upload/v1622832222/IMG_20200621_162523_ilbqfo.jpg",
          nome: "VICENTE PEIXOTO",
          cargo: "Diretor Presidente",
        },
        {
          id: 2,
          image:"https://res.cloudinary.com/dddogptqy/image/upload/v1622831812/T15C3PGRX-UTVF7U2HY-f29cae633a79-512_j2htpc.jpg",
          nome: "VINÍCIUS EDUARDO",
          cargo: "Diretor de Projetos",
        },
        {
          id: 3,
          image:"https://res.cloudinary.com/dddogptqy/image/upload/v1622832250/foto_jlzr35.png",
          nome: "LUCAS SANTOS",
          cargo: "Diretor Financeiro",
        },
        {
          id: 4,
          image:"https://res.cloudinary.com/dddogptqy/image/upload/v1622832992/T15C3PGRX-U01N64P14UU-11f2b0249f37-512_ueuirb.jpg",
          nome: "LUIS ALVES",
          cargo: "Diretor de Marketing",
        },
        {
          id: 5,
          image:"https://res.cloudinary.com/dddogptqy/image/upload/v1622832445/T15C3PGRX-U017SFUC0M7-3df62b41eacb-512_mptoea.jpg",
          nome: "EDUARDO MOSCA",
          cargo: "Desenvolvedor Web",
        }
      ]

      useEffect(() => {

        setExplorers(Member);
        console.log(explorers)
      }, []);

    return (
      <div className="Members">
          {
            explorers.map((val,key) => {
              return (
                <div className="Explorer">
                  <div className="Explorer-img">
                    <a href="#">
                      <Image cloudName="oencpayh" publicId={val.image}/>
                      <div className="Cover">
                        <div className="Cover-inner"></div>
                      </div>
                    </a>
                  </div>
                  <div className="Explorer-about">
                    <h5>{val.nome}</h5>
                    <p>{val.cargo}</p>
                  </div>
                </div>
              );
          })
          }
        </div>
    );
  }
  
  export default Members;
  