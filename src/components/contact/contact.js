import "./style.css";

function Contact() {
    return (
      <section className="Contact" id="Contact">
        <div className="Section-4">
        
        <div className="Container">
          <div className="Contact-title">
            <h1>CONTATE NOS</h1>
            <div className="Divider">
              <div className="Line"></div>
              <div className="Div">
                <p>//</p>
              </div>
              <div className="Line"></div>
            </div>
            <p>Pronto para sua nova aquisição? Alguma dúvida? Entre em contato conosco, estamos ansiosos para resolver seu problema!</p>
          </div>
          <div className="Contact-content">
            <div className="Contact-form">
              <form>
                <input placeholder="Seu nome"></input>
                <input placeholder="E-mail"></input>
                <input placeholder="Assunto"></input>
                <textarea placeholder="Mensagem"></textarea>
                <button>Enviar</button>
              </form>
            </div>
            <div className="Contact-info">
              <h3>NOSSA SEDE</h3>
              <p>Aguardamos sua visita! Basta agendar e o atenderemos com prazer no horário em que estiver disponível!</p>

              <div>
                <span>
                  <img src="https://wit.ucsf.edu/sites/g/files/tkssra606/f/wysiwyg/briefcase-solid.png"></img>
                  <strong>Associação Acens de Tecnologia</strong>
                </span>
              </div>
              <div>
    <span>
      <img src="https://www.pngkit.com/png/full/48-480191_how-to-set-use-white-google-map-pin.png"></img>
      <p>Av. Dr. Silas Munguba, 1700 - UECE Campus do Itaperi</p>
    </span>
  </div>
              <div>
    <p>Departamento de Pós-Graduação em Computação - Sala 8</p>
  </div>
              <div>
    <span>
      <img src="https://faragferreiravieira.com.br/site/imagens/fone2.png"></img>
      <p>(85) 9 9768-3660</p>
    </span>
  </div>
              <div>
    <span>
      <img src="https://cdn.pixabay.com/photo/2013/07/13/01/16/newsgroup-155430_960_720.png"></img>
      <p>contato@acensjr.com</p>
    </span>
  </div>
            </div>
          </div>
        </div>
        </div>
      </section>
    );
  }
  
  export default Contact;