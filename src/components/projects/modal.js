import React, { useEffect, useCallback, useState } from 'react';
/*
import { useSpring, animated } from 'react-spring';
import styled from 'styled-components';
import { MdClose } from 'react-icons/md';
*/


function Modal({ showModal, setShowModal, Id }) {
  /*
  const animation = useSpring({
    config: {
      duration: 250
    },
    opacity: showModal ? 1 : 0,
    transform: showModal ? `translateY(0%)` : `translateY(-100%)`
  });
  */
  const [modal, setModal] = useState([]);

  const Project = [
    {
      id: 1,
      logo:"https://res.cloudinary.com/dddogptqy/image/upload/v1623181049/Prancheta-43_hf4iiq.png",
      nome: "TECSYS JR",
      description: "Empresa Júnior de Engenharia Elétrica da Universidade Federal do Ceará (UFC)",
      details: "A Tecsys Jr é uma empresa formada por alunos de engenharia elétrica da Universidade Federal do Ceará há mais da 20 anos mantém o compromisso de desenvolver as melhores soluções, sempre com dedicação e profissionalismo.",
      images: "",
    },
    {
      id: 2,
      logo:"https://res.cloudinary.com/dddogptqy/image/upload/v1623181057/Captura_de_tela_de_2020-06-26_00-19-44_mhdxee.png",
      nome: "UP ENGENHARIA",
      description: "Somos uma Empresa Júnior de Engenharia Civil da Universidade Estadual do Oeste do Paraná",
      details: "Empresa Júnior de Engenharia Civil da Universidade Estadual do Oeste do Paraná, campus Cascavel, fundada em 2015, formada por acadêmicos do curso de Engenharia Civil, orientados por professores especialistas na área. Buscamos desenvolver os membros da equipe e entregar soluções de qualidade, alcançando a satisfação do cliente.",
      images: "",
    },
    {
      id: 3,
      logo:"https://res.cloudinary.com/dddogptqy/image/upload/v1623181063/Captura_de_tela_de_2020-06-26_00-32-51_c4edkj.png",
      nome: "MESQUITA BOMFIM ADVOCACIA",
      description: "Firma de advocacia full service",
      details: "Firma de advocacia full service, voltada à prevenção de conflitos administrativos e judiciais e à solução eficaz dos litígios contenciosos.",
      images: "",
    },
    {
      id: 4,
      logo:"https://res.cloudinary.com/dddogptqy/image/upload/v1623181071/home_uqzjmo.png",
      nome: "JAO UNIFOR",
      description: "Jornada Acadêmica de Odontologia da Unifor",
      details: "A Jornada Acadêmica de Odontologia da Universidade de Fortaleza (JAO) é o mais antigo evento acadêmico de odontologia do estado do Ceará. Desde 1998, a Coordenação e a Comissão da JAO do Curso de Odontologia realizam essa Jornada Acadêmica.",
      images: "",
    },
    {
      id: 5,
      logo:"https://res.cloudinary.com/dddogptqy/image/upload/v1623181085/login_x7ej6r.png",
      nome: "GAUTAMA DÔJÔ",
      description: "Sistema Financeiro",
      details: "Sistema de controle financeiro com indicadores de movimentações e de pagamentos recorrentes dos alunos/clientes",
      images: "",
    }
  ]

  useEffect(() => {
    setModal(Project[(Id-1)]);
    console.log(Id);
    console.log(modal);
  },[Id]);


  const keyPress = useCallback(
    e => {
      if (e.key === 'Escape' && showModal) {
        setShowModal(false);
      }
    },
    [setShowModal, showModal]
  );

  useEffect(
    () => {
      document.addEventListener('keydown', keyPress);
      return () => document.removeEventListener('keydown', keyPress);
    },
    [keyPress]
  );

  return (
    <>
      {showModal ? (
        <div className="Modal">
            <div className="Modal-content" showModal={showModal}>
                <div className="Container">
                    <div className="Modal-body">
                        <div className="Modal-img" src="" alt="img"></div>
                        <div className="Modal-info">
                            <h2>nome</h2>
                            <div className="Line"></div>
                            <p>algo</p>
                            <p>mais algo</p>
                        </div>
                    </div>
                    <div className="Modal-footer">
                        <p>2021 © Todos os direitos reservados</p>
                    </div>
                </div>
                <div className="Close-modal" aria-label='Close modal' onClick={() => setShowModal(prev => !prev)}></div>
            </div>
        </div>
      ) : null}
    </>
  );
}

export default Modal;
