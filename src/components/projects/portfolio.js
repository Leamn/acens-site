import "./style.css";
import { useEffect, useState} from "react";
import {Image} from 'cloudinary-react';
import Modal from "./modal";

function Portfolio() {

    const [projects, setProjects] = useState([]);

    const Project = [
        {
          id: 1,
          logo:"https://res.cloudinary.com/dddogptqy/image/upload/v1623181049/Prancheta-43_hf4iiq.png",
          nome: "TECSYS JR",
          description: "Empresa Júnior de Engenharia Elétrica da Universidade Federal do Ceará (UFC)",
          details: "A Tecsys Jr é uma empresa formada por alunos de engenharia elétrica da Universidade Federal do Ceará há mais da 20 anos mantém o compromisso de desenvolver as melhores soluções, sempre com dedicação e profissionalismo.",
          images: "",
        },
        {
          id: 2,
          logo:"https://res.cloudinary.com/dddogptqy/image/upload/v1623181057/Captura_de_tela_de_2020-06-26_00-19-44_mhdxee.png",
          nome: "UP ENGENHARIA",
          description: "Somos uma Empresa Júnior de Engenharia Civil da Universidade Estadual do Oeste do Paraná",
          details: "Empresa Júnior de Engenharia Civil da Universidade Estadual do Oeste do Paraná, campus Cascavel, fundada em 2015, formada por acadêmicos do curso de Engenharia Civil, orientados por professores especialistas na área. Buscamos desenvolver os membros da equipe e entregar soluções de qualidade, alcançando a satisfação do cliente.",
          images: "",
        },
        {
          id: 3,
          logo:"https://res.cloudinary.com/dddogptqy/image/upload/v1623181063/Captura_de_tela_de_2020-06-26_00-32-51_c4edkj.png",
          nome: "MESQUITA BOMFIM ADVOCACIA",
          description: "Firma de advocacia full service",
          details: "Firma de advocacia full service, voltada à prevenção de conflitos administrativos e judiciais e à solução eficaz dos litígios contenciosos.",
          images: "",
        },
        {
          id: 4,
          logo:"https://res.cloudinary.com/dddogptqy/image/upload/v1623181071/home_uqzjmo.png",
          nome: "JAO UNIFOR",
          description: "Jornada Acadêmica de Odontologia da Unifor",
          details: "A Jornada Acadêmica de Odontologia da Universidade de Fortaleza (JAO) é o mais antigo evento acadêmico de odontologia do estado do Ceará. Desde 1998, a Coordenação e a Comissão da JAO do Curso de Odontologia realizam essa Jornada Acadêmica.",
          images: "",
        },
        {
          id: 5,
          logo:"https://res.cloudinary.com/dddogptqy/image/upload/v1623181085/login_x7ej6r.png",
          nome: "GAUTAMA DÔJÔ",
          description: "Sistema Financeiro",
          details: "Sistema de controle financeiro com indicadores de movimentações e de pagamentos recorrentes dos alunos/clientes",
          images: "",
        }
      ]

      useEffect(() => {
        setProjects(Project);
      }, []);

      const [modalInUse, setModalInUse] = useState();

      const getModal = (id) => {
        setModalInUse(id);
      };

      const [showModal, setShowModal] = useState(false);

      const openModal = () => {
        setShowModal(prev => !prev);
      };

    return (
      <div className="Portfolio">
          {
            projects.map((val,key) => {
              return (
                <div className="Project-item">
                    <button onClick={() => {
                      getModal(val.id);
                      openModal();
                    }}>
                      <Image cloudName="oencpayh" publicId={val.logo}/>
                      <div className="Caption">
                      <div className="Caption-content">
                        <h2>{val.nome}</h2>
                        <div className="Divider"></div>
                        <p>{val.description}</p>
                      </div>
                      </div>
                    </button>
                    <Modal Id={modalInUse} showModal={showModal} setShowModal={setShowModal} />
                </div>
              );
          })
          }
        </div>
    );
  }

  export default Portfolio;
