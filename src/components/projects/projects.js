import Portfolio from "./portfolio";
import "./style.css";

function Projects() {
    return (
      <section className="Projects" id="Projects">
          <div className="Container">
            <h1>ÚLTIMOS PROJETOS</h1>
            <div className="Divider">
              <div className="Line"></div>
              <div className="Div">
                <p>//</p>
              </div>
              <div className="Line"></div>
            </div>
            <Portfolio />
          </div>
      </section>
    );
  }

  export default Projects;
